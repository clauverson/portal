<?php

class Create extends Conn{

    private $tabela;
    private $dados;
    private $result;

    private $Create;

    private $Conn;

    /**
     * Classe responsavel pela união das classes que montam a query e retornam o ultimo id cadastrado
     * @param [string] $Tabela      [Nome da tabela onde os dados serão inseridos]
     * @param [array de string] array $Dados [Recebe atraves de um array o nome da coluna e o valor a ser inserido ('nomeColuna' => 'valor')]
     */
    public function ExeCreate($Tabela, array $Dados){
        $this->tabela = (string) $Tabela;
        $this->dados = $Dados;

        $this->getSyntax();
        $this->Execute();
    }

    /**
     * Retorna o ultimo id inserido
     */
    public function getResult(){
        return $this->result;
    }

    //_______| private methods |___________
    /**
     * Classe responsavel por preparar a query
     */
    private function Connect(){
        $this->Conn = parent::getConn();
        $this->Create = $this->Conn->prepare($this->Create);
    }

    /**
     * Classe responsavel por usar os dados recebidos para montar a query
     */
    private function getSyntax(){
        $fields = implode(', ', array_keys($this->dados));
        $places = ':' . implode(', :', array_keys($this->dados));
        $this->Create = "INSERT INTO {$this->tabela} ({$fields}) VALUES ({$places})";
    }

    /**
     * Classe responsavel por executar a query e retornar o ultimo id inserido caso não ocorra falha
     */
    private function Execute(){
        $this->Connect();
        try{
            $this->Create->execute($this->dados);
            $this->result = $this->Conn->lastInsertId();
        }catch (PDOException $e){
            $this->result = null;
            CLErro("<b>Erro ao cadastrar:</b> {$e->getMessage()}", $e->getCode());
        }
    }
}

?>