<?php

// CONFIGURAÇÃO DE DIRETORIO ##########
$_SERVER['DOCUMENT_ROOT'] .= '/online/clauverson.com/';

define('RAIZ', $_SERVER['DOCUMENT_ROOT']);

// CONFIGRAÇÕES DO SITE ####################
define('HOST', '31.170.166.50'); // alterar para mysql.hostinger.com.br antes de enviar ao servidor, atual = 31.170.166.50
define('USER', 'u567822032_cosmo');
define('PASS', 'pA7i1@d3a4');
define('DBSA', 'u567822032_cosmo');

// AUTO LOAD DE CLASSES ####################
function __autoload($Class) {

    $cDir = ['Conn', 'Helpers', 'Models'];
    $iDir = null;
    
    foreach($cDir as $dirName){
        if(!$iDir && file_exists(__DIR__ . "/{$dirName}/{$Class}.class.php") && !is_dir(__DIR__ . "/{$dirName}/{$Class}.class.php")){
            include_once(__DIR__ . "/{$dirName}/{$Class}.class.php");
            $iDir = true;
        }
    }
    
    if(!$iDir){
        trigger_error("Não foi possível incluir {$Class}.class.php", E_USER_ERROR);
        die;
    }
}

// TRATAMENTO DE ERROS #####################
//CSS constantes :: Mensagens de Erro
define('CL_SUCCESS', 'success');
define('CL_INFO', 'info');
define('CL_WARNING', 'warning');
define('CL_DANGER', 'danger');

//WSErro :: Exibe erros lançados :: Front
function CLErro($ErrMsg, $ErrNo, $ErrDie = null) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? CL_INFO : ($ErrNo == E_USER_WARNING ? CL_WARNING : ($ErrNo == E_USER_ERROR ? CL_DANGER : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">{$ErrMsg}<span class=\"ajax_close\"></span></p>";

    if ($ErrDie):
        die;
    endif;
}

//PHPErro :: personaliza o gatilho do PHP
function PHPErro($ErrNo, $ErrMsg, $ErrFile, $ErrLine) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? CL_INFO : ($ErrNo == E_USER_WARNING ? CL_WARNING : ($ErrNo == E_USER_ERROR ? CL_DANGER : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">";
    echo "<b>Erro na Linha: #{$ErrLine} ::</b> {$ErrMsg}<br>";
    echo "<small>{$ErrFile}</small>";
    echo "<span class=\"ajax_close\"></span></p>";

    if ($ErrNo == E_USER_ERROR):
        die;
    endif;
}

set_error_handler('PHPErro');
